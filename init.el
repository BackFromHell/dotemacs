(add-to-list 'load-path "~/.emacs.d/Asgardian/")
(require 'a-packages) ; Packages configuration.

;;| YaSnippet : Insert code prototype.
;;  ---------------------------------------------------------------------
(use-package yasnippet
  :ensure t
  :diminish yas-minor-mode
  :init
  ;; Shut yas up! Disables startup noise
  (setq-default yas-verbosity 0)
  )

(use-package yasnippet-snippets
  :ensure t
  :defer t
  :requires yasnippet

  ;;:init
  ;;(setq yas-snippet-dirs (append yas-snippet-dirs
  ;;'("~/.emacs.d/elpa/yasnippet-snippets-20190623.939/snippets")))
  )

;;| Company : Auto completion.
;;  ---------------------------------------------------------------------
(use-package company
  :ensure t
  :requires yasnippet
  :after yasnippet

  :init
  (add-hook 'after-init-hook 'global-company-mode)

  :diminish company-mode
  :custom
  ;; no delay no autocomplete
  (company-idle-delay 0)
  ;;(company-minimum-prefix-length 2)
  (company-tooltip-limit 20)

  )
;; Sort company candidates by statistics
(use-package company-statistics
  :ensure t
  :requires company
  :after company
  :commands company-statistics-mode
  :init (company-statistics-mode t))

(use-package company-try-hard
  :straight t
  :bind (:map company-active-map
              ("C-z" . company-try-hard)))

;; (use-package compdef
;;   :ensure t)
;; (compdef
;;  :modes #'emacs-lisp-mode
;;  :company '(company-dabbrev company-capf)
;;  :capf #'pcomplete-completions-at-point)


;;| Programming Mode : The Global programming mode.
;;  ---------------------------------------------------------------------
(add-hook 'prog-mode-hook
          (lambda ()
            ;; Activate YaSnippet into all programming modes.
            (add-to-list 'company-backends 'company-yasnippet)
            (yas-reload-all)
            (yas-minor-mode)
            ))


;;| eLisp : The eLisp's Mode.
;;  ---------------------------------------------------------------------
(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (add-to-list 'company-backends 'company-elisp)
            ))
