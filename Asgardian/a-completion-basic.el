;;; a-completion-basic.el --- Read and auto completion  -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Thierry Leurent

;; Author: Thierry Leurent <thierry.leurent@asgardian.be>
;; Keywords: functions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; (use-package yasnippet
;;   :ensure t
;;   :requires (company)
;;   :after company
;;   :diminish yas-minor-mode
;;   :init (progn
;;           (add-to-list 'company-backends 'company-yasnippet)
;;           ;;(global-set-key (kbd "M-/") 'company-yasnippet)
;;           (setq yas-snippet-dirs
;;                 '("~/.emacs.d/snippets"                 ;; personal snippets
;;                   ))
;;           )
;;   :config
;;   (yas-global-mode t)
;;   )

(use-package yasnippet
  :ensure t
  :diminish yas-minor-mode
  :init (yas-global-mode t))

(use-package yasnippet-snippets
  :ensure t
  :defer t
  :requires yasnippet
  ;;:init
  ;;(setq yas-snippet-dirs (append yas-snippet-dirs
  ;;'("~/.emacs.d/elpa/yasnippet-snippets-20190623.939/snippets")))
  )
;;| Company Mode : The auto completion mode
;; _____________________________________________________________________________
(use-package company
  :defer t
  :init (add-hook 'after-init-hook 'global-company-mode)
  :config (progn
            ;; From https://www.monolune.com/configuring-company-mode-in-emacs/
            ;; No delay in showing suggestions.
            (setq company-idle-delay 0)
            ;; Show suggestions after entering one character.
            (setq company-minimum-prefix-length 1)
            ;;
            (setq company-selection-wrap-around t)
            (setq company-transformers '(company-sort-by-occurrence))
            ;; Use tab key to cycle through suggestions.
            ;; ('tng' means 'tab and go')
            (company-tng-configure-default)
            (add-to-list 'company-backends 'company-files)
            )
  :diminish company-mode



  )


(use-package company-quickhelp
  :ensure t
  :requires (company)
  :after (company)
  :init (with-eval-after-load 'company
          (company-quickhelp-mode))
  :hook
  (company-mode . company-quickhelp-mode)
  )
;; With use-package:
(use-package company-box
  :requires (company)
  :after (company)
  :hook (company-mode . company-box-mode))
;; Sort company candidates by statistics
(use-package company-statistics
  :ensure t
  :requires (company)
  :after company
  :config (company-statistics-mode))
;; ; Completion for Math symbols
(use-package company-math
  :ensure t
  :requires (company)
  :after company
  :config
  ;; Add backends for math characters
  (add-to-list 'company-backends 'company-math-symbols-unicode)
  (add-to-list 'company-backends 'company-math-symbols-latex))
(use-package company-flx
  :requires (company)
  :after company
  :config
  (company-flx-mode +1))

(use-package company-dict
  :requires (company)
  :after company
  :config;; Where to look for dictionary files. Default is ~/.emacs.d/dict
  (setq company-dict-dir (concat user-emacs-directory "dict/"))

  ;; Optional: if you want it available everywhere
  (add-to-list 'company-backends 'company-dict)

  ;; Optional: evil-mode users may prefer binding this to C-x C-k for vim
  ;; omni-completion-like dictionary completion
  (define-key evil-insert-state-map (kbd "C-x C-k") 'company-dict))
;;| Prescient
;; _____________________________________________________________________________
(use-package prescient
  :ensure t
  ;;:after no-littering
  :config (prescient-persist-mode 1))

(use-package company-prescient
  :ensure t
  :after company
  :init (company-prescient-mode 1))

(provide 'a-completion-basic)
;;; a-completion-basic.el ends here
