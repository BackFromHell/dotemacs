;;; a-packages.el --- Read and auto completion  -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Thierry Leurent

;; Author: Thierry Leurent <thierry.leurent@asgardian.be>
;; Keywords: Packages, MELPA, ELPA.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;;| ELPA & MELPA repositories
;; _____________________________________________________________________________
(require 'package)
(package-initialize)
(unless (assoc-default "gnu" package-archives)
  (add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t))
(unless (assoc-default "melpa" package-archives)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t))
(unless (assoc-default "org" package-archives)
  (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t))

;;| Use the Emacs package manager
;; _____________________________________________________________________________
(setq custom-file "~/.emacs.d/custom-settings.el")
(load custom-file t)
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(setq use-package-verbose t)
(setq use-package-always-ensure t)
(require 'use-package)
(use-package auto-compile
  :config (auto-compile-on-load-mode))
(setq load-prefer-newer t)



(provide 'a-packages)
;;; a-packages.el ends here
